FROM --platform=amd64 node:22-bullseye AS builder

ARG USE_CUSTOM_SDKS=false
ARG JS_SDK_REPO="https://github.com/matrix-org/matrix-js-sdk.git"
ARG JS_SDK_BRANCH="master"
ARG VERSION

RUN apt-get update && apt-get install -y git dos2unix

RUN git clone https://gitlab.mim-libre.fr/EOLE/eole-3/services/element/element-web-sources.git /src
RUN cd /src && \
    git checkout ${VERSION}

WORKDIR /src

RUN dos2unix /src/scripts/docker-link-repos.sh
RUN dos2unix /src/scripts/docker-package.sh
RUN /src/scripts/docker-link-repos.sh
RUN yarn --network-timeout=200000 install
RUN /src/scripts/docker-package.sh

RUN cp /src/config.sample.json /src/webapp/config.json

FROM nginx:alpine-slim

RUN apk add jq moreutils

COPY --from=builder /src/webapp /app

COPY --from=builder /src/docker/nginx-templates/default.conf.template /etc/nginx/templates/
COPY --from=builder /src/docker/docker-entrypoint.d/18-load-element-modules.sh /docker-entrypoint.d/

RUN sed -i -e 's,/var/run/nginx.pid,/tmp/nginx.pid,' /etc/nginx/nginx.conf

RUN chown -R nginx:0 /var/cache/nginx /etc/nginx
RUN chmod -R g+w /var/cache/nginx /etc/nginx

RUN rm -rf /usr/share/nginx/html \
  && ln -s /app /usr/share/nginx/html

USER nginx

ENV ELEMENT_WEB_PORT=80

HEALTHCHECK --start-period=5s CMD wget --retry-connrefused --tries=5 -q --wait=3 --spider http://localhost:$ELEMENT_WEB_PORT/config.json
