# element-container-image

## Initial mirroring the upstream repository

git clone --bare https://github.git element-sources element-web-sources
cd element-web-sources
git push -f git@gitlab.mim-libre.fr:EOLE/eole-3/services/element/element-web-sources.git refs/heads/main
git push -f git@gitlab.mim-libre.fr:EOLE/eole-3/services/element/element-web-sources.git refs/heads/\* refs/tags/\*

ou

git clone --mirror https://github.com/element-hq/element-web.git element-web-sources
cd element-web-sources
git remote set-url --push origin git@gitlab.mim-libre.fr:EOLE/eole-3/services/element/element-web-sources.git
git push --mirror

## Update the mirror from upstream

git fetch -p origin
git push -f git@gitlab.mim-libre.fr:EOLE/eole-3/services/element/element-web-sources.git refs/heads/main
git push -f git@gitlab.mim-libre.fr:EOLE/eole-3/services/element/element-web-sources.git refs/heads/\* refs/tags/\*

ou

git fetch -p origin
git remote set-url --push origin git@gitlab.mim-libre.fr:EOLE/eole-3/services/element/element-web-sources.git
git push --mirror
